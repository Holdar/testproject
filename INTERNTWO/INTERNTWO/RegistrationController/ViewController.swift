//
//  ViewController.swift
//  INTERNTWO
//
//  Created by Murtazaliev Shamil on 25/03/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var userLogin = UserLogin()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTextField.isSecureTextEntry = true
        //        print(userLogin.array.count)
    }
    
    @IBAction func loginButton(_ sender: Any) {
//        if userLogin.array.count == 0 {
//            showAlert(title: "Ошибка при вводе данных", message: "Неверный пароль, введите правильный пароль и попробуйте снова")
//        }else{
//        if loginTextField.text == userLogin.array[0]{
//        }else{
//            showAlert(title: "Ошибка при вводе данных", message: "Неверный пароль, введите правильный пароль и попробуйте снова")
//        }
//        if passwordTextField.text == userLogin.array[1]{
//        }else{
//            showAlert(title: "Ошибка при вводе данных", message: "Неверный логин, введите правильный логин и попробуйте снова")
//        }
//    }
//        performSegue(withIdentifier: "segueInTable", sender: nil)
        userLogin.getLoginAndPassword(login: loginTextField.text!, password: passwordTextField.text!, success: { (success) in
            if success == "ready" {
                self.performSegue(withIdentifier: "segueInTable", sender: nil)
            }
        }) { (failure) in
            switch failure {
            case "noLogin": self.showAlert(title: "Ошибка при вводе данных", message: "Неверный логин, введите правильный логин и попробуйте снова")
            case "noPassword": self.showAlert(title: "Ошибка при вводе данных", message: "Неверный пароль, введите правильный пароль и попробуйте снова")
            default:
                self.showAlert(title: "Ошибка при вводе данных", message: "Попробуйте снова")
            }
        }
    }
    
    @IBAction func registrationButton(_ sender: Any) {
        performSegue(withIdentifier: "segueInRegistration", sender: nil)
    }
    
    func showAlert(title: String, message: String) -> Any {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        return self.present(alert, animated: true)
    }
}

