//
//  RegistrationVC.swift
//  INTERNTWO
//
//  Created by Murtazaliev Shamil on 26/03/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit

class RegistrationVC: UIViewController {
    
    var userLogin = UserLogin()
    
    let viewController = ViewController()
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var nameLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTextField.isSecureTextEntry = true
    }
    
    @IBAction func segueInViewController(_ sender: Any) {
        userDefault.set("", forKey: "login")
        userDefault.set("", forKey: "password")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func registrationButton(_ sender: Any) {
        if loginTextField.text == "" || passwordTextField.text == "" || nameLabel.text == ""{
            showAlert(title: "Ошибка при вводе данных", message: "Вы ввели не все данные, введите все данные и попробуйте снова")
        }else{
            userLogin.postLoginAndPassword(login: loginTextField.text!, password: passwordTextField.text!)
        }
//        if passwordTextField.text == ""{
//            showAlert(title: "Ошибка при вводе данных", message: "Вы ввели не все данные, введите все данные и попробуйте снова")
//        }else{
//            userDefault.set(passwordTextField.text, forKey: "password")
//        }
//        if nameLabel.text == ""{
//            showAlert(title: "Ошибка при вводе данных", message: "Вы ввели не все данные, введите все данные и попробуйте снова")
//        }
//
//        print(userLogin.array[0])
//        print(userLogin.array[1])
        performSegue(withIdentifier: "segueInTable", sender: nil)
    }
    
    func showAlert(title: String, message: String) -> Any {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        return self.present(alert, animated: true)
    }
}
