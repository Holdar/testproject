//
//  DetailsUserVC.swift
//  INTERNTWO
//
//  Created by Murtazaliev Shamil on 02/04/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit

class DetailsUserVC: UIViewController {

    @IBOutlet weak var detailsUserImage: UIImageView!
    @IBOutlet weak var detailsUserLogin: UILabel!
    @IBOutlet weak var detailsUserName: UILabel!
    @IBOutlet weak var detailsUserLocation: UILabel!
    @IBOutlet weak var detailsUserPublicRepositories: UILabel!
    @IBOutlet weak var detailsUserObservers: UILabel!
    @IBOutlet weak var detailsUserObservable: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailsUserImage.frame.size = CGSize(width: view.frame.width, height: view.frame.width)
    }
}
