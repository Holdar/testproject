//
//  TableViewCell.swift
//  INTERNTWO
//
//  Created by Murtazaliev Shamil on 26/03/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userLorem: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
