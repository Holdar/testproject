//
//  Alamofire.swift
//  INTERNTWO
//
//  Created by Murtazaliev Shamil on 29/03/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation
import Alamofire

class Alamofire {
    func getUsers(url: URL, success: @escaping ([[String: Any]]) -> Void, failure: @escaping (String) -> Void) {
        request(url).responseJSON { response in
            if response.result.isSuccess {
                if let result = response.result.value as? [[String: Any]]{
                    success(result)
                }else {
                    failure("noAs")
                }
            }else {
                failure("noResponse")
            }
        }
    }
    
    func getUser(url: URL, success: @escaping ([String: Any]) -> Void, failure: @escaping (String) -> Void) {
        request(url).responseJSON { response in
            if response.result.isSuccess {
                if let result = response.result.value as? [String: Any]{
                    success(result)
                }else {
                    failure("noAs")
                }
            }else {
                failure("noResponse")
            }
        }
    }
}
