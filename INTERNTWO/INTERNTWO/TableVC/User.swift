//
//  User.swift
//  INTERNTWO
//
//  Created by Murtazaliev Shamil on 29/03/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import Foundation

struct User {
    var userName: String
    var userEmail: String
    var userLorem: String
    var userAvatar: String
    var url: String
    
}
