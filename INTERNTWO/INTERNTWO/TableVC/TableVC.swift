//
//  TableVC.swift
//  INTERNTWO
//
//  Created by Murtazaliev Shamil on 26/03/2019.
//  Copyright © 2019 Murtazaliev Shamil. All rights reserved.
//

import UIKit
import SDWebImage

var alamofire = Alamofire()
var user: User!
var users = [User]()

class TableVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        pars()
    }
    
    func pars(){
        alamofire.getUsers(url: URL(string: "https://api.github.com/users")!, success: { (result) in
            for usersItem in result {
                let userItem = User(userName: usersItem["login"]! as! String, userEmail: usersItem["url"]! as! String, userLorem: usersItem["repos_url"]! as! String, userAvatar: usersItem["avatar_url"]! as! String, url: usersItem["url"] as! String)
                users.append(userItem)
            }
            self.tableView.reloadData()
        }) { (failure) in
            switch failure {
            case "noRespons": self.showAlert(title: "Eror", message: "noRespons")
            case "noAs": self.showAlert(title: "Eror", message: "noAs")
            default:
                self.showAlert(title: "Eror", message: "Eror")
            }
        }
    }
    
    @IBAction func goOut(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension TableVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TableViewCell else {
            fatalError("")
        }
        cell.nameUser.text = users[indexPath.row].userName
        cell.userEmail.text = users[indexPath.row].userEmail
        cell.userLorem.text = users[indexPath.row].userLorem
        cell.userImage.sd_setImage(with: URL(string: (users[indexPath.row].userAvatar)), completed: nil)
        return cell
    }
    
    func showAlert(title: String, message: String) -> Any {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        return self.present(alert, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueInDetailsUser"{
            if let userDetails = segue.destination as? DetailsUserVC {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    print(users[indexPath.row].url)
                    alamofire.getUser(url: URL(string: users[indexPath.row].url)!, success: { (result) in
                        userDetails.detailsUserImage!.sd_setImage(with: URL(string: (result["avatar_url"] as? String)!), completed: nil)
                        userDetails.detailsUserLogin.text = result["login"] as? String
                        userDetails.detailsUserName.text = result["name"] as? String
                        userDetails.detailsUserLocation.text = result["location"] as? String
                        userDetails.detailsUserPublicRepositories.text = String(result["public_repos"] as! Int)
                        userDetails.detailsUserObservers.text = String(result["followers"] as! Int)
                        userDetails.detailsUserObservable.text = String(result["following"] as! Int)
                    }) { (failure) in
                        switch failure {
                        case "noRespons": self.showAlert(title: "Eror", message: "noRespons")
                        case "noAs": self.showAlert(title: "Eror", message: "noAs")
                        default:
                            self.showAlert(title: "Eror", message: "Eror")
                        }
                    }
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueInDetailsUser", sender: nil)
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "DetailsUser") as! DetailsUserVC
//        if let indexPath = self.tableView.indexPathForSelectedRow {
//                                print(users[indexPath.row].url)
//            alamofire.getUser(url: URL(string: users[indexPath.row].url)!, success: { (result) in
//                print(result["avatar_url"]! as! String)
//                vc.detailsUserImage.sd_setImage(with: URL(string: result["avatar_url"] as! String), completed: nil)
//                vc.detailsUserLogin.text = result["login"] as? String
//                                    vc.detailsUserName.text = result["name"] as? String
//                                    vc.detailsUserLocation.text = result["location"] as? String
//                                    vc.detailsUserPublicRepositories.text = result["public_repos"] as? String
//                                    vc.detailsUserObservers.text = result["followers"] as? String
//                                    vc.detailsUserObservable.text = result["following"] as? String
//                                }) { (failure) in
//                                    switch failure {
//                                    case "noRespons": self.showAlert(title: "Eror", message: "noRespons")
//                                    case "noAs": self.showAlert(title: "Eror", message: "noAs")
//                                    default:
//                                        self.showAlert(title: "Eror", message: "Eror")
//                                    }
//                                }
//                            }
//        performSegue(withIdentifier: "segueInDetailsUser", sender: nil)
//    }
}
